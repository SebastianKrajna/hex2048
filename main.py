import sys
from PySide2 import QtGui, QtCore, QtWidgets
from PySide2.QtWidgets import QMainWindow, QGraphicsView, QWidget, QGridLayout, QLabel, QAction, QMenuBar, QMenu, QApplication


from Hex import Hex
from ButtonBox import ButtonBox
from GameBoard import GameBoard
from ConfigWindow import ConfigWindow
from QRCItem import QRCItem

import json
   


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.setupWidgets()
        self.setupSignals()
        self.setupMenu()
   
    def setupWidgets(self):
        
        self.qrcItemOne = QRCItem()
        self.qrcItemTwo = QRCItem(reverse=True)
        
        self.button_box = ButtonBox()
        self.button_box.setFixedSize(230, 645)
        
        self.boardOneScoreLabel = QLabel("Score: 0")
        self.boardOneScoreLabel.setFixedHeight(80)
        self.boardOneScoreLabel.setFont(QtGui.QFont('Comic Sans', 30, QtGui.QFont.Bold))
        
        self.boardOne = GameBoard(self, mouseable=True)
        self.boardOne.addItem(self.qrcItemOne)
        self.viewOne = QGraphicsView(self.boardOne)
        self.viewOne.setRenderHints(QtGui.QPainter.Antialiasing | QtGui.QPainter.HighQualityAntialiasing)
        self.viewOne.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.viewOne.setFixedSize(555, 630)

        self.boardTwoScoreLabel = QLabel("Score: 0")
        self.boardTwoScoreLabel.setFixedHeight(80)
        self.boardTwoScoreLabel.setFont(QtGui.QFont('Comic Sans', 30, QtGui.QFont.Bold))
        
        self.boardTwo = GameBoard(self)
        self.boardTwo.addItem(self.qrcItemTwo)
        self.viewTwo = QGraphicsView(self.boardTwo)
        self.viewTwo.setRenderHints(QtGui.QPainter.Antialiasing | QtGui.QPainter.HighQualityAntialiasing)
        self.viewTwo.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.viewTwo.setFixedSize(555, 630)
        
        self.boardTwoScoreLabel.setVisible(False)
        self.viewTwo.setVisible(False)
        
        widget = QWidget()
               
        layout = QGridLayout(widget)
        layout.setColumnStretch(2, 1) 
        layout.addWidget(self.button_box, 1, 0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignCenter)
        layout.addWidget(self.boardOneScoreLabel, 0, 1, QtCore.Qt.AlignCenter)
        layout.addWidget(self.viewOne, 1, 1, QtCore.Qt.AlignCenter)
        layout.addWidget(self.boardTwoScoreLabel, 0, 2, QtCore.Qt.AlignCenter)
        layout.addWidget(self.viewTwo, 1, 2, QtCore.Qt.AlignCenter)
        
        self.setCentralWidget(widget)
    
    
    def setupMenu(self):
        self.menu_bar = QMenuBar()
        
        self.OptionsMenu = QMenu("&Options")
        
        self.multiplayer_action = QAction("Multiplayer")
        self.sizeplus_action    = QAction("Size +")
        self.sizeminus_action   = QAction("Size -")
        self.reset_action       = QAction("Reset")
        self.exit_action        = QAction("Exit")
        
        self.multiplayer_action.setCheckable(True)
        self.multiplayer_action.setChecked(False)
        
        self.multiplayer_action.triggered.connect(self.multiplayerAction)
        self.sizeplus_action.triggered.connect(lambda: self.changeSizeAction(1))
        self.sizeminus_action.triggered.connect(lambda: self.changeSizeAction(-1))
        self.reset_action.triggered.connect(self.resetMethod)
        self.exit_action.triggered.connect(self.exitMethod)
        
        self.OptionsMenu.addAction(self.multiplayer_action)
        self.OptionsMenu.addAction(self.sizeplus_action)
        self.OptionsMenu.addAction(self.sizeminus_action)
        self.OptionsMenu.addAction(self.reset_action)
        self.OptionsMenu.addAction(self.exit_action)
        
        
        self.FileMenu = QMenu("&File")
        
        self.save_action = QAction("Save")
        self.load_action = QAction("Load")
        self.undo_action = QAction("Undo")
        
        self.save_action.triggered.connect(self.saveMethod)
        self.load_action.triggered.connect(self.loadMethod)
        self.undo_action.triggered.connect(self.undoMethod)
        
        self.FileMenu.addAction(self.save_action)
        self.FileMenu.addAction(self.load_action)
        self.FileMenu.addAction(self.undo_action)
        
        
        self.NetworkMenu = QMenu("&Network")
        
        self.config_action = QAction("Config")
        self.find_enemies = QAction("Find Enemies")
        self.connect_action = QAction("Connect")
        
        self.config_action.triggered.connect(self.openConfigWindow)
        # self.find_enemies.tiggered.connect()
        # self.connect_action.tiggered.connect()
        
        self.NetworkMenu.addAction(self.config_action)
        self.NetworkMenu.addAction(self.find_enemies)
        self.NetworkMenu.addAction(self.connect_action)
        
        
        self.menu_bar.addMenu(self.OptionsMenu)
        self.menu_bar.addMenu(self.FileMenu)
        self.menu_bar.addMenu(self.NetworkMenu)
        self.setMenuBar(self.menu_bar)
        
    
    def setupSignals(self):
        self.button_box.size_signal.connect(self.changeSizeMethod)
        self.button_box.reset_signal.connect(self.resetMethod)
        self.button_box.save_signal.connect(self.saveMethod)
        self.button_box.load_signal.connect(self.loadMethod)
        self.button_box.undo_signal.connect(self.undoMethod)
        self.button_box.control_signal.connect(self.controlMethod)
        self.button_box.exit_signal.connect(self.exitMethod)
        
        
    def changeSizeMethod(self, s):
        self.boardOne.changeSize(s)
        self.boardOne.addItem(QRCItem())
          
        
    def resetMethod(self):
        self.boardOne.reset()
        self.boardOne.addItem(QRCItem())
        
        
    def saveMethod(self):
        self.boardOne.saveLastState()
     
     
    def loadMethod(self):
        self.boardOne.loadLastState()        
                                                                   
    
    def controlMethod(self, direction):
        if direction == 'up left':      self.boardOne.move((0, 1), rev=False) 
        if direction == 'down right':   self.boardOne.move((0, -1), rev=True)
        if direction == 'up right':     self.boardOne.move((-1, 1), rev=False)     
        if direction == 'down left':    self.boardOne.move((1, -1), rev=True)    
        if direction == 'left':         self.boardOne.move((1, 0), rev=False)   
        if direction == 'right':        self.boardOne.move((-1, 0), rev=True)
    
    
    def undoMethod(self):
        self.boardOne.undo()
    
    
    def exitMethod(self):
        self.close()
    
    
    def changeSizeAction(self, s):
        self.boardOne.changeSize(s + self.boardOne.getBoardSize())
        self.boardOne.addItem(QRCItem())
        
    
    def openConfigWindow(self):
        self.conf = ConfigWindow()
        self.conf.show()
        
    
    def multiplayerAction(self):
        if self.multiplayer_action.isChecked() == True:
            self.multiplayer_action.setChecked(True)
            self.boardTwoScoreLabel.setVisible(True)
            self.viewTwo.setVisible(True)
            self.setFixedSize(1400, 770)
        else:
            self.multiplayer_action.setChecked(False)
            self.boardTwoScoreLabel.setVisible(False)
            self.viewTwo.setVisible(False)
            self.setFixedSize(830, 770)
            
            
    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Q: self.boardOne.move((0, 1), rev=False) 
        if event.key() == QtCore.Qt.Key_C: self.boardOne.move((0, -1), rev=True)     
        if event.key() == QtCore.Qt.Key_E: self.boardOne.move((-1, 1), rev=False)      
        if event.key() == QtCore.Qt.Key_Z: self.boardOne.move((1, -1), rev=True)      
        if event.key() == QtCore.Qt.Key_A: self.boardOne.move((1, 0), rev=False)   
        if event.key() == QtCore.Qt.Key_D: self.boardOne.move((-1, 0), rev=True)    




if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = MainWindow() 
    form.setFixedSize(830, 770)
    form.show()
    app.exec_()