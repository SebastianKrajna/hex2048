from PySide2 import QtWidgets, QtGui, QtCore

from PySide2.QtWidgets import QGraphicsItem
from PySide2.QtCore import QPointF
from PySide2.QtGui import QColor, QPen
import numpy as np
import random


class Hex(QGraphicsItem):
    
    colors = {0:    QColor(255, 255, 255),
              2:    QColor(255, 235, 200),
              4:    QColor(255, 215, 135),
              8:    QColor(255, 180,   0),
              16:   QColor(255, 170,   0),
              32:   QColor(255,  155,  0),
              64:   QColor(255,  145,  0),
              128:  QColor(255,  133,  0),
              256:  QColor(255,  120,  0),
              512:  QColor(255,  105,  0),
              1024: QColor(255,   95,  0),
              2048: QColor(255,   80,  0),
              }
    
    def __init__(self, center, size):
        super(Hex, self).__init__()
        
        self.size = size
        self.x, self.y = center
        self.number = random.choice([0,0,2,4])
         
        self.pen = QPen(QtCore.Qt.SolidLine)
        self.pen.setColor(QtGui.Qt.black)
        self.pen.setWidth(self.size/10) 
        
        self.font = QtGui.QFont('Comic Sans', size*0.4, QtGui.QFont.Bold)
        self.font.setWeight(300)
        self.font.setStyleStrategy(QtGui.QFont.PreferAntialias)

        self.hex = QtGui.QPolygonF(self.gen_points_around())
          
            
    def boundingRect(self):
        return self.hex.boundingRect()

    
    def paint(self, painter, option, widget=None):
        painter.setOpacity(1.0)
        try:
            painter.setBrush(self.colors[self.number])
        except:
            painter.setBrush(QColor(25, 180,  120))
        painter.setPen(self.pen)
        painter.setFont(self.font)
        
        painter.drawPolygon(self.hex) 
        
        if self.number != 0:
            painter.drawText(self.x-self.size, self.y-self.size, self.size*2, self.size*2, QtCore.Qt.AlignCenter, str(self.number))
            
            
    def gen_points_around(self):
        pts = []
        for i in range(6):
            angle_deg = 60 * i - 30
            angle_rad = np.deg2rad(angle_deg)
            p_x = self.x + self.size * np.cos(angle_rad)
            p_y = self.y + self.size * np.sin(angle_rad)
            pts.append(QPointF(p_x, p_y))
        pts.append(pts[0])
        return pts
    
    
    def __add__(self, other):
        self.number += other.number
        other.number = 0
        
    def __iadd__(self, other):
        self.number += other.number
        other.number = 0
        return self
        
    def __eq__(self, num):
        return self.number == num
        
    def __ne__(self, num):
        return self.number != num
        
    def __str__(self):
        return str(self.number)
    
    def set_number(self, s):
        self.number = s
    
    def get_number(self):
        return self.number
