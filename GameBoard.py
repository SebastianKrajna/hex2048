from PySide2 import QtGui, QtCore, QtWidgets
from PySide2.QtWidgets import QApplication, QGraphicsTextItem, QGraphicsScene, QFileDialog

from Hex import Hex
from QRCItem import QRCItem

import numpy as np
import random
import time
from termcolor import cprint

import xml.etree.ElementTree as ET


class GameBoard(QGraphicsScene):
    def __init__(self, parent, mouseable=False):
        super(GameBoard, self).__init__()  
        
        self.parent = parent
        self.mouseable = mouseable
        
        self.hex_size = 48
        self.board_size = 3
        self.game_score = 0
        
        self.can_move = True

        self.hex_dict = dict()
        self.game_hist = []
        
        self.genHexs()
        
        
    def changeSize(self, s):
        self.game_score = 0
        self.clear()
        self.hex_dict.clear()
        self.game_hist.clear()
        self.board_size = s  
        self.hex_size = 3*48/self.board_size
        self.genHexs()
    
    
    def getBoardSize(self):
        return self.board_size
        

    def setCanMove(self, s):
        self.can_move = s


    def reset(self):
        self.game_score = 0
        self.clear()
        self.hex_dict.clear()
        self.game_hist.clear()
        self.genHexs()
        
    
    def saveLastState(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filePath, s = QFileDialog.getSaveFileName(self.parent, "QFileDialog.getSaveFileName()","history","XML Files (*.xml)", options=options)
        if filePath:
            if '.xml' not in filePath:
                filePath += '.xml'
            
            root = ET.Element('Game')
            
            bsize = ET.Element('boardSize')
            bsize.text = str(self.board_size)
                        
            root.append(bsize)
            
            for ghist in self.game_hist:
                
                hround = ET.Element('round')
                root.append(hround)
                
                hroundgs = ET.Element('roundScore')
                hroundgs.text = str(ghist[-1])
                hround.append(hroundgs)
                
                for (x,y,v) in ghist[:-1]:
                    h = ET.Element('hex')
                    hround.append(h)
                    col   = ET.SubElement(h, 'col')
                    row   = ET.SubElement(h, 'row')
                    score = ET.SubElement(h, 'number')

                    col.text = str(x)
                    row.text = str(y)
                    score.text = str(v)
            
            tree = ET.ElementTree(root)
 
            with open(filePath, 'wb') as file:
                tree.write(file)
    

    def loadLastState(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        filePath, _ = QFileDialog.getOpenFileName(self.parent,"QFileDialog.getOpenFileName()", "history.xml","XML Files (*.xml)", options=options)
        
        self.game_hist.clear()
        
        if filePath:
            tree = ET.parse(filePath)
            root = tree.getroot()
            
            s = int(root.find('boardSize').text)
            self.changeSize(s)
            self.addItem(QRCItem())
            
            for hround in root.findall('round'):
                self.game_score = hround.find('roundScore').text
                
                for elem in hround.findall('hex'):
                    x = int(elem.find('col').text)
                    y = int(elem.find('row').text)
                    score = int(elem.find('number').text) 
                    self.hex_dict[x,y].set_number(score)
                
                self.saveToHistory()
                    
                self.update()
                self.parent.boardOneScoreLabel.setText(f"Score: {self.game_score}")
                QApplication.processEvents()
                self.printGameHistory('emulate')
                time.sleep(0.2)
     
         
    def undo(self):
        if self.game_hist:
            gh = self.game_hist.pop()
            self.game_score = gh[-1]
            for (x,y,v) in gh[:-1]:
                self.hex_dict[x,y].set_number(v)

            self.parent.boardOneScoreLabel.setText(f"Score: {self.game_score}")
            self.printGameHistory('undo')
            self.update()
                     
             
    def genHexs(self):
        
        hex_position = np.zeros((2*self.board_size-1, 2*self.board_size-1))
        for y in range(2*self.board_size-1):
            for x in range(2*self.board_size-1):
                if (y+x+2)**2 > (self.board_size)**2 and (y+x+2)**2 < (3*self.board_size)**2:
                    hex_position[x, y] = 1
                    
        width_hex  = np.sqrt(3) * self.hex_size + self.hex_size/5
        height_hex = 2 * self.hex_size + self.hex_size/5
        
        w = width_hex
        h = height_hex

        for y in range(2*self.board_size-1):
            w = width_hex
            for num in hex_position[y]:
                if num == 0:
                    w += width_hex/2
            
            for x in range(2*self.board_size-1):
                if hex_position[x, y] == 1:                    
                    item = Hex((w, h), self.hex_size)
                    self.hex_dict[(x,y)] = item
                    self.addItem(item)
                    
                    w += width_hex
            h += 3/4*height_hex

        self.parent.boardOneScoreLabel.setText(f"Score: {self.game_score}")
        self.saveToHistory()
    
    
    def saveToHistory(self):
        l = []
        for (x,y), v in self.hex_dict.items():
            l.append((x,y,v.get_number()))
        l.append(self.game_score)
        self.game_hist.append(l)
  
                
    def genNewHexs(self):
        empty_hex = []
        for (x,y), v in self.hex_dict.items():
            if v == 0:
                empty_hex.append(v)
        
        leh = len(empty_hex)
        if leh > 0:
            amount = np.random.randint(1, leh//2) if leh > 3 else leh
            choice = np.random.choice(empty_hex, size=amount)
            for c in choice:
                c.set_number(random.choice([2,4]))            
                
        
    def move(self, direction, rev=False):
        
        if(self.can_move == True):
            hd = self.hex_dict.items() if rev == False else reversed(self.hex_dict.items())
            
            for (x,y), v in hd:
                newx = x + direction[0]
                newy = y + direction[1]
                while (newx, newy) in self.hex_dict:
                    if self.hex_dict[newx, newy] == 0:
                        newx += direction[0]
                        newy += direction[1]
                    elif v == 0 or v == self.hex_dict[newx, newy]:
                        if v != 0 and self.hex_dict[newx, newy] != 0:
                            self.game_score += int(2*self.hex_dict[x, y].get_number()) 
                        self.parent.boardOneScoreLabel.setText(f"Score: {self.game_score}")
                        self.hex_dict[x,y] += self.hex_dict[newx, newy]
                    else:
                        break
            
            self.genNewHexs()
            self.saveToHistory()
            self.update()
            self.printGameHistory(direction) 
            self.checkEndGame()                
                        
            
    def checkEndGame(self):
        
        end_game = True
        
        for (x,y), v in self.hex_dict.items():
            if v == 2048:
                self.can_move = False
                for v in self.hex_dict.values():
                    v.setOpacity(0.2)
                t = QGraphicsTextItem("YOU WIN!")
                t.setFont(QtGui.QFont('Comic Sans', 60, QtGui.QFont.Bold))
                t.setPos(self.width()//2 - t.boundingRect().width()//2, self.height()//2 - t.boundingRect().height()//2)
                self.addItem(t)
                self.update()
            if v == 0:
                end_game = False
            
        direct = [(0,1), (0,-1), (-1,1), (1,-1), (1,0), (-1,0)]
        
        for (x,y), v in self.hex_dict.items():
            for (i, j) in direct:
                if (x+i, y+j) in self.hex_dict:
                    if v == self.hex_dict[x+i, y+j]:
                        end_game = False

        if end_game:
            self.can_move = False
            for v in self.hex_dict.values():
                v.setOpacity(0.2)
            t = QGraphicsTextItem("GAME OVER!")
            t.setFont(QtGui.QFont('Comic Sans', 60, QtGui.QFont.Bold))
            t.setPos(self.width()//2 - t.boundingRect().width()//2, self.height()//2 - t.boundingRect().height()//2)
            self.addItem(t)
            self.update()    
                
                
    def printGameHistory(self, direction):
        
        n = ''
        xold = 0
        yold = 0
        
        cprint('-'*(self.board_size*2+15), 'red', end='\n')
        
        if direction == (0, 1):    cprint('\t\t up left', 'cyan', end='\n') 
        if direction == (0, -1):   cprint('\t\t down right', 'cyan', end='\n') 
        if direction == (-1, 1):   cprint('\t\t up right', 'cyan', end='\n')      
        if direction == (1, -1):   cprint('\t\t down left', 'cyan', end='\n')     
        if direction == (1, 0):    cprint('\t\t left', 'cyan', end='\n')    
        if direction == (-1, 0):   cprint('\t\t right', 'cyan', end='\n') 
        if direction == 'emulate': cprint('\t\t emulate', 'cyan', end='\n')
        if direction == 'undo':    cprint('\t\t undo', 'cyan', end='\n')
        
        for (x,y), v in self.hex_dict.items():
            if y - yold != 0:
                n += '\n'
                cprint(n, 'yellow')
                xold = 0
                yold = y
                n = '' if y%2!=0 else ' '
                n = '  ' * (y-self.board_size+1)
            n += '  ' * (x-xold) + str(v.get_number()) # 
            xold = x
        cprint(n, 'yellow', end='\t')
        cprint(f'Score: {self.game_score}', 'cyan', end='\n\n') 
    
                
    def mousePressEvent(self, event):
        self.xPressed = event.scenePos().x()
        self.yPressed = event.scenePos().y()
    
    
    def mouseReleaseEvent(self, event):
        self.xReleased = event.scenePos().x()
        self.yReleased = event.scenePos().y()         
        
        if self.mouseable:
            angle = np.arctan2(self.yPressed-self.yReleased, self.xPressed-self.xReleased)
            angle = int(angle * 180 / np.pi)
            
            if angle >= 165 or angle < -165:    self.move((-1, 0), rev=True)        
            elif angle >= 90 and angle < 165:   self.move((-1, 1), rev=False)  
            elif angle >= 15 and angle < 90:    self.move((0, 1), rev=False)     
            elif angle >= -15 and angle < 15:   self.move((1, 0), rev=False)       
            elif angle >= -90 and angle < -15:  self.move((1, -1), rev=True)       
            elif angle >= -165 and angle < -90: self.move((0, -1), rev=True)  
            else: print(angle)