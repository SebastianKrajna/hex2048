from PySide2 import QtWidgets, QtGui, QtCore

from PySide2.QtWidgets import QWidget, QLineEdit, QPushButton, QGridLayout
from PySide2.QtCore import Signal


class ButtonBox(QWidget):
    
    reset_signal    = Signal()
    load_signal     = Signal()
    save_signal     = Signal()
    undo_signal  = Signal()
    exit_signal     = Signal()
    control_signal  = Signal(str)
    size_signal     = Signal(int)
    
    def __init__(self):
        super(ButtonBox, self).__init__()
        
        self.up_left    = QPushButton('Up Left')
        self.up_right   = QPushButton('Up Right')
        self.left       = QPushButton('Left')
        self.right      = QPushButton('Right')
        self.down_left  = QPushButton('Down Left')
        self.down_right = QPushButton('Down Right')
        
        self.buttonHeight = 40
        
        self.up_left.setFixedSize(97, self.buttonHeight)
        self.up_right.setFixedSize(97, self.buttonHeight)
        self.left.setFixedSize(97, self.buttonHeight)
        self.right.setFixedSize(97, self.buttonHeight)
        self.down_left.setFixedSize(97, self.buttonHeight)
        self.down_right.setFixedSize(97, self.buttonHeight)
        
        controlLayout = QGridLayout()
        controlLayout.addWidget(self.up_left, 0,0)
        controlLayout.addWidget(self.up_right, 0,1)
        controlLayout.addWidget(self.left, 1,0)
        controlLayout.addWidget(self.right, 1,1)
        controlLayout.addWidget(self.down_left, 2,0)
        controlLayout.addWidget(self.down_right, 2,1)
        
        self.minus = QPushButton('-')
        self.plus  = QPushButton('+')
        self.text  = QLineEdit('3')
        
        self.minus.clicked.connect(lambda: self.changeSize(-1))
        self.plus.clicked.connect(lambda: self.changeSize(1))
        
        self.minus.setFixedSize(self.buttonHeight, self.buttonHeight)
        self.text.setFixedSize(80, self.buttonHeight)
        self.plus.setFixedSize(self.buttonHeight, self.buttonHeight)
        
        self.text.setAlignment(QtCore.Qt.AlignCenter)
        self.text.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("^1[0-5]|[2-9]$")))
        
        self.reset_button       = QPushButton('Reset')
        self.load_button        = QPushButton('Load')
        self.save_button        = QPushButton('Save')
        self.autoplay_button    = QPushButton('Autoplay')      
        self.undo_button     = QPushButton('Undo')
        self.exit_button        = QPushButton('Exit')

        self.reset_button.setFixedSize(200, self.buttonHeight)  
        self.load_button.setFixedSize(97, self.buttonHeight) 
        self.save_button.setFixedSize(97, self.buttonHeight)
        self.autoplay_button.setFixedSize(200, self.buttonHeight)
        self.undo_button.setFixedSize(200, self.buttonHeight)
        self.exit_button.setFixedSize(200, self.buttonHeight)  
        
        sizeLayout = QGridLayout()
        sizeLayout.addWidget(self.minus, 0, 0)
        sizeLayout.addWidget(self.text, 0, 1, QtCore.Qt.AlignCenter)
        sizeLayout.addWidget(self.plus, 0, 2)
        
        saveLoadLayout = QGridLayout()
        saveLoadLayout.addWidget(self.load_button, 0, 0, QtCore.Qt.AlignLeft)
        saveLoadLayout.addWidget(self.save_button, 0, 1, QtCore.Qt.AlignRight)
        
        layout = QGridLayout()
        
        layout.addLayout(controlLayout, 0, 0)
        layout.addLayout(sizeLayout, 1, 0)
        layout.addWidget(self.reset_button, 2, 0, QtCore.Qt.AlignTop)
        layout.addLayout(saveLoadLayout, 3, 0, QtCore.Qt.AlignTop) 
        layout.addWidget(self.autoplay_button, 4, 0, QtCore.Qt.AlignTop)
        layout.addWidget(self.undo_button, 5, 0, QtCore.Qt.AlignTop)
        layout.setRowStretch(5, 1) 
        layout.addWidget(self.exit_button, 6, 0, QtCore.Qt.AlignBottom)
        
        self.setLayout(layout)
        
        self.reset_button.clicked.connect(self.resetMethod)
        self.load_button.clicked.connect(self.loadMethod)
        self.save_button.clicked.connect(self.saveMethod)
        self.undo_button.clicked.connect(self.undoMethod)
        self.exit_button.clicked.connect(self.exitMethod)
        
        self.up_left.clicked.connect(lambda: self.controlMethod("up left"))
        self.up_right.clicked.connect(lambda: self.controlMethod("up right"))
        self.left.clicked.connect(lambda: self.controlMethod("left"))
        self.right.clicked.connect(lambda: self.controlMethod("right"))
        self.down_left.clicked.connect(lambda: self.controlMethod("down left"))
        self.down_right.clicked.connect(lambda: self.controlMethod("down right"))
        
        self.text.textChanged.connect(self.sizeMethod)
        
    def changeSize(self, val=0):
        s = int(self.text.text()) + val
        if s < 2: s = 2
        elif s > 15: s = 15
        self.text.setText(str(s))
        
    def sizeMethod(self):
        try:
            s = int(self.text.text())
            self.size_signal.emit(s)
        except ValueError:
            print (" s = '' ")
    
    def undoMethod(self):
        self.undo_signal.emit()
    
    def resetMethod(self):
        self.reset_signal.emit()
        
    def loadMethod(self):
        self.load_signal.emit()
    
    def saveMethod(self):
        self.save_signal.emit()
        
    def exitMethod(self):
        self.exit_signal.emit()
        
    def controlMethod(self, direction):
        self.control_signal.emit(direction)
