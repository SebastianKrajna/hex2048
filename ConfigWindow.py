from PySide2 import QtCore, QtGui
from PySide2.QtWidgets import QDialog, QPushButton, QWidget, QGridLayout, QLabel, QLineEdit
from PySide2.QtCore import Qt
import json

        
class ConfigWindow(QDialog):
    def __init__(self, parent=None):
        super(ConfigWindow, self).__init__(parent)
        
        self.setWindowTitle("Config")
        self.setupWidgets()
        
        self.conf = {}
    
    def setupWidgets(self):
        
        self.name_label             = QLabel("Name:")
        self.ip_address_label       = QLabel("IP address:")
        self.enemy_name_label       = QLabel("Enemy name:")
        self.enemy_ip_address_label = QLabel("Enemy IP address:") 
        self.port_label             = QLabel("Port:")
        self.board_size_label       = QLabel("Board size:")
        self.best_scores_label      = QLabel("Best scores:")
        self.best_score1_label      = QLabel("300")
        self.best_score2_label      = QLabel("200")
        self.best_score3_label      = QLabel("100")
         
        self.name_edit              = QLineEdit('')
        self.ip_address_edit        = QLineEdit('')         
        self.enemy_name_edit        = QLineEdit('')
        self.enemy_ip_address_edit  = QLineEdit('')
        self.port_edit              = QLineEdit('')
        self.board_size_edit        = QLineEdit('')
        
        self.readConfig()
        
        self.save_button = QPushButton("Save")
        self.save_button.clicked.connect(self.saveConfig)
          
        layout = QGridLayout()
        
        layout.addWidget(self.name_label,           1, 0, Qt.AlignRight)
        layout.addWidget(self.ip_address_label,     2, 0, Qt.AlignRight)
        layout.addWidget(self.enemy_name_label,     3, 0, Qt.AlignRight)
        layout.addWidget(self.enemy_ip_address_label, 4, 0, Qt.AlignRight)
        layout.addWidget(self.port_label,           5, 0, Qt.AlignRight)
        layout.addWidget(self.board_size_label,     6, 0, Qt.AlignRight)
        layout.addWidget(self.best_scores_label,    7, 0, Qt.AlignRight)
        
        layout.addWidget(self.name_edit,           1, 1, Qt.AlignLeft)
        layout.addWidget(self.ip_address_edit,     2, 1, Qt.AlignLeft)
        layout.addWidget(self.enemy_name_edit,     3, 1, Qt.AlignLeft)
        layout.addWidget(self.enemy_ip_address_edit, 4, 1, Qt.AlignLeft)
        layout.addWidget(self.port_edit,           5, 1, Qt.AlignLeft)
        layout.addWidget(self.board_size_edit,     6, 1, Qt.AlignLeft)
        layout.addWidget(self.best_score1_label,    7, 1, Qt.AlignRight)        
        layout.addWidget(self.best_score2_label,    8, 1, Qt.AlignRight)
        layout.addWidget(self.best_score3_label,    9, 1, Qt.AlignRight)
        layout.addWidget(self.save_button,         11,1, Qt.AlignRight)
        
        self.setLayout(layout)
        
    def readConfig(self):
        try:
            with open('config.json', 'r') as f:
                self.conf = json.load(f)
                
                self.name_edit.setText(self.conf['Name'])
                self.ip_address_edit.setText(self.conf['IP address'])
                self.enemy_name_edit.setText(self.conf['Enemy name'])
                self.enemy_ip_address_edit.setText(self.conf['Enemy IP address'])
                self.port_edit.setText(self.conf['Port'])
                self.board_size_edit.setText(self.conf['Board size'])
                self.best_score1_label.setText(self.conf['Best scores'][0])
                self.best_score2_label.setText(self.conf['Best scores'][1])
                self.best_score3_label.setText(self.conf['Best scores'][2])
        except:
            pass
        
    def saveConfig(self):
        self.conf = {
            "Name": self.name_edit.text(),
            "IP address": self.ip_address_edit.text(),
            "Enemy name": self.enemy_name_edit.text(),
            "Enemy IP address": self.enemy_ip_address_edit.text(),
            "Port": self.port_edit.text(),
            "Board size": self.board_size_edit.text(),
            "Best scores": [self.best_score1_label.text(),
                            self.best_score2_label.text(),
                            self.best_score3_label.text()]
        }
        
        with open('config.json', 'w+') as f:
            json.dump(self.conf, f)
            
        self.close()